package ictgradschool.industry.lab10.ex02;

import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;

/**
 * Main program for Lab 10 Ex 2, which should print out a table showing the frequency of all alphanumeric characters
 * in a text block.
 */
public class ExerciseTwo {

    /**
     * Loops through the given String and builds a Map, relating each alphanumeric character in the String (key)
     * with how many times that character occurs in the string (value). Ignore case.
     *
     * @param text the text to analyze
     * @return a mapping between characters and their frequencies in the text
     */
    private Map<Character, Integer> getCharacterFrequencies(String text) {

        // Ignore case. We need only deal with uppercase letters now, after this line.
        text = text.toUpperCase();


        Map<Character, Integer> frequencies = new TreeMap<>();

        // Loop through all characters in the given string
        for (char c : text.toCharArray()) {

            // If c is alphanumeric...
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {

                if(frequencies.containsKey(c)){
                    frequencies.put(c, frequencies.get(c) + 1);
                }else{
                    frequencies.put(c, 1);
                }


            }

        }
        String s = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 0; i < s.length() ; i++) {
            char ch = s.charAt(i);
            if (!frequencies.containsKey(ch)) {
                frequencies.put(ch, 0);
            }
        }



        return frequencies;

    }

    /**
     * Prints the given map in a user-friendly table format.
     *
     * @param frequencies the map to print
     */
    private void printFrequencies(Map<Character, Integer> frequencies) {

        System.out.println("Char:\tFrequencies:");
        System.out.println("--------------------");

        // TODO Loop through the entire map and print out all the characters (keys)
        // TODO and their frequencies (values) in a table.
        for (Map.Entry<Character, Integer> entry : frequencies.entrySet()) {
            System.out.println("\'" + entry.getKey() + "\'  " + entry.getValue());

        }

    }

    /**
     * Main program flow. Do not edit.
     */
    private void start() {
        Map<Character, Integer> frequencies = getCharacterFrequencies(Constants.TEXT);
        printFrequencies(frequencies);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
